/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastraritem;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author gin
 */
public class ItemTableModel extends AbstractTableModel{

    private List<Item> itens;
    private List<String> cabecalho;

    public ItemTableModel() {
        itens       = new ArrayList<>();
        cabecalho   = new ArrayList<>();
        
        cabecalho.add("Id");
        cabecalho.add("Nome");
        cabecalho.add("Valor");
    }

    public ItemTableModel(List<Item> itens, List<String> cabecalho) {
        this.itens = itens;
        this.cabecalho = cabecalho;
    }
    
    
    
    
    
    @Override
    public int getRowCount() {
        return itens.size();
    }

    @Override
    public int getColumnCount() {
        return cabecalho.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch( columnIndex ){
            case 0:
                return itens.get(rowIndex).getId();
            case 1:
                return itens.get(rowIndex).getNome();
            case 2:
                return itens.get(rowIndex).getValor();
                
        }
        return null;
    }

    @Override
    public String getColumnName(int column) {
        return cabecalho.get(column);
    }
    
    public List<Item> getItens() {
        return itens;
    }

    public void setItens(List<Item> itens) {
        this.itens = itens;
    }
    
    
    
}
