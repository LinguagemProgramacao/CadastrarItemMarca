/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastrarmarca;

/**
 *
 * @author patrick
 */
public class Marca {
    private Integer id;
    private String nome;
    private Integer idItem;

    public Marca() {
    }

    public Marca(Integer id, String nome, Integer idItem) {
        this.id = id;
        this.nome = nome;
        this.idItem = idItem;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getIdItem() {
        return idItem;
    }

    public void setIdItem(Integer idItem) {
        this.idItem = idItem;
    }
    
    
}
