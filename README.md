<h1>Projeto de Cadastro de Itens</h1>
<b>OBS:</b> Projeto funcional apenas ao ignorar o autocommit.

Para o perfeito funcionamento do exercicio, fique atento a configuraçao:

Nos atributos localizados no inicio da classe CadastroListagem.

	private static final String prefix = "jdbc:mysql://";
	private static final String host   = "localhost";
	private static final String port   = ":3306";// aqui coloca a porta usado pelo mysql
	private static final String dbName = "/bancodb";// aqui coloca o nome do banco anteriormente criado
	private static final  String user = "root"; // aqui coloca o nome do usuario do banco	
	private static final  String pass = ""; // aqui coloca a senha do banco, se estiver usando uma


<h2>Metodos importantes</h2>

Para ter a conexao, este metodo e usado. Toda vez que precisar, chame por <b>getConn()</b>.
```
public  Connection getConn() {
        try {
            
            conn = DriverManager.getConnection(url, user, pass);            
            
        } catch (SQLException sQLException) {
        }finally{
            return conn;
        }
    }
```

Para criar uma instancia da classe Marca e passar a atual instancia da CadastroListagem, adicione as duas linhas dentro do botao de cadastro da <b>CadatroListagem</b>: <b>src>castroitem>CadastroListagem</b>

```
CadastroMarca marca = new CadastroMarca(this); // com este this, passamos a instancia da primeira classe dentro do construtor da segunda
marca.setVisible(true);
```

Em seguida, na classe <b>CadastroMarca</b>, crie o seguinte construtor:<b>src>castromarca>CadastroMarca</b>
```
public CadastroMarca(Object item) {
        initComponents();
        this.classeitem = (CadastroListagem) item;
    }
```
Obs: e necessario o atributo do tipo <b>CadatroListagem</b>
```
public CadastroListagem classeitem;
```
